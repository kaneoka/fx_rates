require 'open-uri'
require 'yaml'
require 'pdf-reader'
require 'spreadsheet'
require 'fileutils'
require 'net/sftp'

class TransferUpdatedFxRate

  def run
    pdf_file_path = acquire_pdf_file
    p "pdf_file_path: " + pdf_file_path

    tts, unit = acquire_tts_from_pdf(pdf_file_path)
    tts_per_unit = tts/unit
    p "tts(unit): " + tts.to_s + "(" + unit.to_s + ")"
    p "tts/unit: " + (tts_per_unit).to_s

    updated_xls_file_path = update_tts_in_xls(tts_per_unit)
#    transfer_xls_via_sftp(updated_xls_file_path)
  end

  target_date = Date.today - 1
  
  DATE_STRING_MMDD = target_date.strftime("%m%d")
  DATE_STRING_YYYYMMDD = target_date.strftime("%Y%m%d")

  def acquire_pdf_file
    config = YAML.load_file('config.yaml')
    target_config = config['target']
    
    target_host = target_config['host']
    target_filename = target_config['filename']['prefix'] + DATE_STRING_MMDD + target_config['filename']['extension']
    target_file_path = target_host + target_config['filepath'] + target_filename
    
    download_filename = target_filename
    download_dir_path = "./output/" + DATE_STRING_YYYYMMDD
    download_file_path = download_dir_path + "/" + download_filename
    
    FileUtils.mkdir_p(download_dir_path) unless FileTest.exist?(download_dir_path)
    
    open(download_file_path, 'wb') do |output|
      open(target_file_path) do |data|
        output.write(data.read)
      end
    end
    return download_file_path
  end

  def acquire_tts_from_pdf(pdf_file_path)
    country = "KOREA"
    reader = PDF::Reader.new(pdf_file_path)
    text = reader.pages[0].text
    results = {}
    text.each_line do |line|
      if line.include?(country)
        match_line = line.split(" ") 
        results["UNIT"] = match_line[2]
        results["TTS"] = match_line[4]
      end
    end

    tts = results["TTS"].to_f    
    unit = results["UNIT"].to_f
    return tts, unit
  end

  def update_tts_in_xls(tts_per_unit)
    config = YAML.load_file('config.yaml')
    template_config = config['template']

    book_name = template_config['filename'] 
    book_path = "./template/" + book_name
    
    new_book_name = "fx_" + DATE_STRING_MMDD + ".xls"
    new_book_dir_path = "./output/" + DATE_STRING_YYYYMMDD
    new_book_path = new_book_dir_path + "/" + new_book_name
    
    FileTest.exist?(new_book_path) ? FileUtils.rm(new_book_path) : FileUtils.cp(book_path, new_book_path)
    book = Spreadsheet.open(book_path)
    sheet = book.worksheet(0)
    sheet.rows[1][5] = tts_per_unit
    book.write(new_book_path)
    return new_book_path
  end

  def transfer_xls_via_sftp(xls_file_path)
    config = YAML.load_file('config.yaml')
    sftp_setting = config['sftp_setting']

    options = {
      password: sftp_setting[:password],
      port: sftp_setting[:port]
      }
    host = sftp_setting[:host]
    username = sftp_setting[:username]
    remote_file_path = sftp_setting[:remote_file_path]
    local_file_path = xls_file_path
    Net::SFTP.start(host, username, options) do |sftp|
      sftp.upload(local_file_path, remote_file_path)
    end
  end

end

TransferUpdatedFxRate.new.run
